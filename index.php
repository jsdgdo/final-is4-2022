<?php
  require_once __DIR__ . '/vendor/autoload.php';

  //Clase que contiene el script de carga
  $loader = new Final\Loader();
  $loader->loadItems();
  
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Final Primera José Delgado</title>
  <style>
    table {border-collapse: collapse;}
    td, th {border: 1px solid black; padding: 2px 4px;}
  </style>
</head>
<body>
  <h1>Compras por cliente</h1>
  <h2>Clientes que más compraron</h2>
  <table>
    <thead>
      <th>Cliente</th>
      <th>Monto (USD)</th>
    </thead>
    <tbody>
    <?php foreach ($loader->getBigSpenders() as $row) {?>
    <tr>
      <td><?php echo $row['nombre']; ?></td>
      <td><?php echo $row['monto']; ?></td>
    </tr>
    <?php }  ?>
    </tbody>
  </table>
  <h2>Países que más compraron</h2>
  <table>
    <thead>
      <th>País</th>
      <th>Monto (USD)</th>
    </thead>
    <tbody>
    <?php foreach ($loader->getBigCountries() as $row) {?>
    <tr>
      <td><?php echo $row['nombre']; ?></td>
      <td><?php echo $row['monto']; ?></td>
    </tr>
    <?php }  ?>
    </tbody>
  </table>
  <h2>Fechas en que más se facturó</h2>
  <table>
    <thead>
      <th>Fecha</th>
      <th>Monto (USD)</th>
    </thead>
    <tbody>
    <?php foreach ($loader->getBigDates() as $row) {?>
    <tr>
      <td><?php echo $row['fecha']; ?></td>
      <td><?php echo $row['monto']; ?></td>
    </tr>
    <?php }  ?>
    </tbody>
  </table>
  <h2>Clientes que nunca compraron</h2>
  <table>
    <thead>
      <th>Cliente</th>
    </thead>
    <tbody>
    <?php foreach ($loader->getNoSpenders() as $row) {?>
    <tr>
      <td><?php echo $row['nombre']; ?></td>
    </tr>
    <?php }  ?>
    </tbody>
  </table>
  <h2>Clientes que menos compraron</h2>
  <table>
    <thead>
      <th>Cliente</th>
      <th>Monto (USD)</th>
    </thead>
    <tbody>
    <?php foreach ($loader->getSmallSpenders() as $row) {?>
    <tr>
      <td><?php echo $row['nombre']; ?></td>
      <td><?php echo $row['monto']; ?></td>
    </tr>
    <?php }  ?>
    </tbody>
  </table>
</body>
</html>

  