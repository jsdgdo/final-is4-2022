<?php 

namespace Final;

class Vehiculo 
{
  private $circulaPor;
  private $cantidadRuedas;
  /**
   * Class constructor.
   */
  public function __construct(string $cp, int $cr)
  {
    $this->circulaPor = $cp;
    $this->cantidadRuedas = $cr;
  }
}
