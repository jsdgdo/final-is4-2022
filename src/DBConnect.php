<?php

  namespace Final;
  
  class DBConnect
  {
    private $conn;
    public function __construct(string $dbname, string $user)
    {
      $this->conn = new \PDO('pgsql:dbname='.$dbname.' host=localhost', $user);
    }  
    public function getConn() {
      return $this->conn;
    }
  }