<?php

  namespace Final; 

  require_once 'DBConnect.php';
  
  class Loader
  {
    private $dbconn;
    private $isLoaded;
    public function __construct()
    {
      $this->dbconn = new DBConnect('finalprimera', 'jose');
      $this->updateLoad();
    }

    public function updateLoad() {
      //checkea si hay registros antes de cargar
      $db = $this->dbconn->getConn();
      $checkQ = 'SELECT * FROM pais WHERE pais_id = 1';
      $check = $db->query($checkQ)->rowCount();
      if ($check == 0) {
        $this->isLoaded = false;
      } else {
        $this->isLoaded = true;
      }
    }

    public function loadItems()
    {
      
      //Carga manual de datos en arreglos para la carga en el sistema
      $paises = ["1, Paraguay", "2, Brasil", "3, Argentina", "4, Chile", "5, Colombia"];
      $clientes = ["1, 1, Base Base", "2, 1, Matiris", "3, 2, Antartica", "4, 2, Sadia", "5, 3, Arcor", "6, 4, TV Chile", "7, 1, Bebidas del Paraguay", "8, 1, GNB", "9, 1, Perfecta Automotores"];
      $compras = [
        "1, 1, 45000, 03-23-2022",
        "2, 3, 55000, 04-23-2022",
        "3, 3, 65000, 04-23-2022",
        "4, 4, 265000, 05-23-2022",
        "5, 4, 7000, 04-23-2022",
        "6, 2, 17000, 06-23-2022",
        "7, 2, 47000, 04-05-2022",
        "8, 5, 32000, 04-05-2022",
        "9, 3, 47000, 06-05-2022",
        "10, 2, 89000, 06-10-2022"
      ];

      if (!$this->isLoaded) {
        $insertPaises = $db->pgsqlCopyFromArray('pais', $paises, ',', '\\\\N', 'pais_id, nombre');
        $insertClientes = $db->pgsqlCopyFromArray('cliente', $clientes, ',', '\\\\N', 'cliente_id, pais_id, nombre');
        $insertCompras = $db->pgsqlCopyFromArray('compra', $compras, ',', '\\\\N', 'compra_id, cliente_id, monto_total, fecha');
        $this->updateLoad();
      }

    }

    public function getBigSpenders() {
      $db = $this->dbconn->getConn();
      $query = 'SELECT (SELECT nombre FROM cliente WHERE cliente.cliente_id = compra.cliente_id), SUM(monto_total) AS monto FROM compra GROUP BY cliente_id  ORDER BY monto DESC LIMIT 3';
      return $db->query($query)->fetchAll();
    }
    public function getBigCountries() {
      $db = $this->dbconn->getConn();

      $query = 'SELECT (SELECT nombre FROM pais WHERE pais.pais_id = cliente.pais_id), SUM(monto_total) AS monto FROM compra INNER JOIN cliente ON compra.cliente_id = cliente.cliente_id INNER JOIN pais ON pais.pais_id=cliente.pais_id GROUP BY cliente.pais_id ORDER BY monto DESC LIMIT 3';
      return $db->query($query)->fetchAll();
    }
    public function getBigDates() {
      $db = $this->dbconn->getConn();

      $query = 'SELECT fecha, SUM(monto_total) AS monto FROM compra GROUP BY fecha ORDER BY monto DESC LIMIT 3';
      return $db->query($query)->fetchAll();
    }
    public function getNoSpenders() {
      $db = $this->dbconn->getConn();
      $query = 'SELECT DISTINCT cliente.cliente_id, cliente.nombre as nombre FROM cliente LEFT OUTER JOIN compra ON cliente.cliente_id = compra.cliente_id ORDER BY cliente.cliente_id DESC LIMIT 3';
      return $db->query($query)->fetchAll();
    }
    public function getSmallSpenders() {
      $db = $this->dbconn->getConn();
      $query = 'SELECT (SELECT nombre FROM cliente WHERE cliente.cliente_id = compra.cliente_id), SUM(monto_total) AS monto FROM compra GROUP BY cliente_id  ORDER BY monto ASC LIMIT 3';
      return $db->query($query)->fetchAll();
    }
  }