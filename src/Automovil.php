<?php 

namespace Final;

require_once 'Vehiculo.php';

class Automovil extends Vehiculo 
{
  private $marca;
  private $modelo;
  /**
   * Class constructor.
   */
  public function __construct(string $marca, int $modelo)
  {
    parent::__construct('tierra', 4);
    $this->marca = $marca;
    $this->modelo = $modelo;
  }
  public function getMarca() {
    return $this->marca;
  }
  public function getModelo() {
    return $this->modelo;
  }
}
