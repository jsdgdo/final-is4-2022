## Para correr
1. Ir al directorio del proyecto
2. Correr `php -S localhost:8080`
3. User para db `jose` sin contraseña
4. Nombre de la db `finalprimera`

## URLs para pruebas
### Ejercicio Principal
[Inicio](http://localhost:8080/)

### Ejercicio Extra
[Extra](http://localhost:8080/extra.php)
